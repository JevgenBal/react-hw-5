import React from 'react';
import { useSelector } from 'react-redux';
import { FormBuy } from 'src/components/FormBuy/FormBuy';
import { Product } from 'src/components/Product/Product';

import styles from './Basket.module.scss';

const getProducts = (state) => state.products;

export const Basket = () => {
  const { items, productsInBasket } = useSelector(getProducts);

  const productsBasket = items.filter((product) =>
    productsInBasket.find((item) => item.id === product.id)
  );

  const emptyBasket = !productsBasket.length;

  return (
    <div className={styles.Basket}>
      {emptyBasket && <h1>Товаров нет</h1>}
      {!emptyBasket && <FormBuy productsBasket={productsBasket} />}
      {!emptyBasket && (
        <ul className={styles.BasketList}>
          {productsBasket.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
