import React from 'react';
import { useSelector } from 'react-redux';
import { Product } from 'src/components/Product/Product';
import styles from './Favorites.module.scss';

const getProducts = (state) => state.products;

export const Favorites = () => {
  const { items, favorites } = useSelector(getProducts);

  const productsFavorites = items.filter((product) =>
    favorites.includes(product.id)
  );

  const emptyFavorites = !productsFavorites.length;

  return (
    <div className={styles.Favorites}>
      {emptyFavorites && <h1>Товаров нет</h1>}
      {!emptyFavorites && (
        <ul className={styles.FavoritesList}>
          {productsFavorites.map((product) => (
            <Product key={product.id} product={product}></Product>
          ))}
        </ul>
      )}
    </div>
  );
};
