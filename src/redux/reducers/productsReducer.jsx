import { createSlice } from '@reduxjs/toolkit';
import { getData } from '../extraReducers/getData';

// default values for state products
const initialState = {
  items: [],
  favorites: [],
  productsInBasket: [],
  error: null,
  status: null,
};

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setFavorites: (state, action) => {
      state.favorites = action.payload;
    },

    setProductsInBasket: (state, action) => {
      state.productsInBasket = action.payload;
    },

    handleFavoritesClick: (state, action) => {
      if (state.favorites.includes(action.payload)) {
        state.favorites = state.favorites.filter((id) => id !== action.payload);
      } else {
        state.favorites.push(action.payload);
      }

      localStorage.setItem('favorites', JSON.stringify(state.favorites));
    },

    handleAddBasketClick: (state, action) => {
      const itemFound = state.productsInBasket.some(
        (item) => item.id === action.payload
      );

      if (itemFound) {
        state.productsInBasket.find(
          (item) => item.id === action.payload
        ).count += 1;
      } else {
        state.productsInBasket.push({ id: action.payload, count: 1 });
      }

      localStorage.setItem('basket', JSON.stringify(state.productsInBasket));
    },

    handleDeleteBasketClick: (state, action) => {
      const itemIndex = state.productsInBasket.findIndex(
        (item) => item.id === action.payload
      );

      if (itemIndex !== -1) {
        if (state.productsInBasket[itemIndex].count > 1) {
          state.productsInBasket[itemIndex].count -= 1;
        } else {
          state.productsInBasket.splice(itemIndex, 1);
        }

        localStorage.setItem('basket', JSON.stringify(state.productsInBasket));
      }
    },
  },

  extraReducers: (builder) => {
    builder.addCase(getData.rejected, (state, action) => {
      state.error = action.payload;
      state.status = 'rejected';
    });
    builder.addCase(getData.pending, (state, action) => {
      state.error = null;
      state.status = 'pending';
    });
    builder.addCase(getData.fulfilled, (state, action) => {
      state.error = null;
      state.status = 'fulfilled';
      state.items = action.payload;
    });
  },
});

export const {
  setFavorites,
  setProductsInBasket,
  handleFavoritesClick,
  handleAddBasketClick,
  handleDeleteBasketClick,
} = productsSlice.actions;
export default productsSlice.reducer;
